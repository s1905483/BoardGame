"""BoardGame URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.conf.urls import url

from bgweb import views

urlpatterns = [
    path('admin/', admin.site.urls),
    url(r'^$', views.home, name='home'),
    #(\d+) mean we would like to pass the number as parameter
    url(r'^game/(\d+)/', views.game_detail, name='game_detail'),
    
    #game catalogue page
    path('category/', views.game_catalogue, name='category'),
    url(r'^category/(\d*)/', views.update_category, name='category_id'),

    path('my_collection/', views.my_collection, name='my_collection'),
    path('create_collection/', views.create_collection, name='create_collection'),
    url(r'edit_collection/(\d+)/', views.edit_collection, name='edit_collection'),
    path('create/collection/api', views.upload_collection, name='upload_collection'),

    path('login/', views.login_page, name='login'),
    path('login/api/', views.login_api, name='login_api'),
    path('register/', views.register_page, name='register'),
    path('register/api/', views.register_api, name='register_api'),
    path('logout/api/', views.logout_api, name='logout_api'),
    url(r'^delete/collection/(\d+)/', views.delete_collection, name="delete_collection"),
    url(r'^collection/(\d+)/', views.collection_detail, name="collection_detail"),
    path('select/collection/api', views.select_collection, name='select_collection'),

    url(r'^add_to_collection/(\d+)/', views.add_to_collection_page, name="add_to_collection_page"),
    url(r'^remove_from_collection/', views.remove_from_collection, name="remove_from_collection"),
    path('profile/', views.user_profile_api, name='profile'),

    path('add/reviews/api/', views.add_reviews_api, name='add_reviews_api'),
    url(r'^search$', views.search, name='search'),
]
