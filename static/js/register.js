function register() {
    var username = document.getElementById('username').value;
    var password = document.getElementById('password').value;
    var confirmPassword = document.getElementById('confirmPassword').value;
    var email = document.getElementById('email').value;

    if (username.length == 0 || password.length == 0 || confirmPassword.length == 0 || email == 0) {
        alert("Should input completely");
        return 0;
    }

    console.log(username + "  " + password);
    if (password != confirmPassword) {
        alert("Two password is not correct");
        return 0;
    }

    var emailreg = /^([a-zA-Z0-9]+[_|\_|\.]?)*[a-zA-Z0-9]+@([a-zA-Z0-9]+[_|\_|\.]?)*[a-zA-Z0-9]+\.[a-zA-Z]{2,3}$/;
    if(!emailreg.test(email)){
        alert("The email is not legal");
        return 0;
    }

    $.ajax({
        type: 'POST',
        dataType: "json",
        url: "/register/api/",
        data: {
            username: username,
            password: password,
            email: email,
        },
        success: function (data) {
            console.log(data);
            if (data['status'] == 'success') {
                console.log(data['message']);
                window.location.href = "/";
            } else {
                alert(data['message']);
            }
        },
        error: function (jqXHR) {
            console.log("Error: " + jqXHR.status);
        }
    });
    return 1;
}