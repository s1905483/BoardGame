function login() {
    var username = document.getElementById('username').value;
    var password = document.getElementById('password').value;

    if (username.length > 0 && password.length > 0) {
        $.ajax({
            type: 'POST',
            dataType: "json",
            url: "/login/api/",
            data: {
                username: username,
                password: password,
            },
            success: function (data) {
                if (data['status'] == 'success') {
                    console.log(data['message']);
                    window.location.href = "/";
                } else {
                    alert(data['message']);
                }
            },
            error: function (jqXHR) {
                console.log("Error: " + jqXHR.status);
            }
        });
    } else {
        alert("Should input username and password both");
    }
}