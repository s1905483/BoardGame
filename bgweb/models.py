from django.db import models
from django.db.models.signals import pre_delete
from django.dispatch.dispatcher import receiver

# Create your models here.


class User(models.Model):
    id = models.AutoField(primary_key=True)
    username = models.CharField(max_length=200, unique=True)
    password = models.CharField(max_length=200)
    email = models.EmailField(unique=True)
    register_date = models.DateTimeField(auto_now_add=True)
    image = models.ImageField(upload_to='portrait', blank=True, null=True, default='default')

    def __str__(self):
        return self.username


class Category(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=40, null=False)

    def __str__(self):
        return self.name


class Game(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=45)
    creator = models.ForeignKey('User', models.SET_NULL, blank=True, null=True)
    expansion = models.ForeignKey('Game', models.SET_NULL, blank=True, null=True, related_name='expension_game')
    description = models.TextField()
    image = models.ImageField(upload_to='gamecover', blank=True, null=True)
    category = models.ManyToManyField('Category')

    def __str__(self):
        return self.name


class Review(models.Model):
    id = models.AutoField(primary_key=True)
    rating = models.IntegerField()
    comment = models.TextField()
    date = models.DateTimeField(auto_now=True)
    user = models.ForeignKey('User', on_delete=models.CASCADE)
    game = models.ForeignKey('Game', on_delete=models.CASCADE)

    def __str__(self):
        return self.comment


class GameCollection(models.Model):
    id = models.AutoField(primary_key=True)
    id_user = models.ForeignKey('User', on_delete=models.CASCADE)
    games = models.ManyToManyField('Game')
    is_favorite = models.BooleanField()
    name = models.CharField(max_length=45)
    image = models.ImageField(upload_to='collectioncover', blank=True, null=True)

    @classmethod
    def create(cls, id_user, id_game):
        collection = cls(id_user=id_user, games=id_game, is_favorite = False)
        return collection
        
    def __str__(self):
        return self.name


@receiver(pre_delete, sender=Game)
def Game_delete(sender, instance, **kwargs):
    instance.image.delete(False)


@receiver(pre_delete, sender=GameCollection)
def GameCollection_delete(sender, instance, **kwargs):
    instance.image.delete(False)
