from . import models
from django.forms import ModelForm
from django import forms

class GameForm(ModelForm):
    class Meta:
        model = models.Game
        fields = '__all__'

class UserForm(forms.Form):
    name = forms.CharField(label='Your name',
                           max_length=100,
                           required=False,
                           widget=forms.TextInput(attrs={'class': 'form-control', 
                           'readonly':'readonly'}))
    email = forms.EmailField(label='Your email',
                            max_length=100,
                            required=False,
                            widget=forms.EmailInput(attrs={'class': 'form-control', 'readonly':'readonly'}))
    password = forms.CharField(label='Current password',
                            max_length=100,
                            strip=False,
                            required=False,
                            widget=forms.PasswordInput(attrs={'class': 'form-control', 'readonly':'readonly'}))
    new_password = forms.CharField(label='New password',
                            max_length=100,
                            strip=False,
                            required=False,
                            widget=forms.PasswordInput(attrs={'class': 'form-control', 'readonly':'readonly'}))
    rep_new_password = forms.CharField(label='Repeat New password',
                            max_length=100,
                            strip=False,
                            required=False,
                            widget=forms.PasswordInput(attrs={'class': 'form-control', 'readonly':'readonly'}))
