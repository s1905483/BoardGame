from django.contrib import admin

# Register your models here.
from .models import *

#Admin interface to Game class
#to go to the admin interface by default is localhost:8000/admin

@admin.register(Category)
class Category(admin.ModelAdmin):
  list_display = ['id', 'name']

@admin.register(Game)
class GameAdmin(admin.ModelAdmin):
  list_display = ['id', 'name']

@admin.register(User)
class UserAdmin(admin.ModelAdmin):
  list_display = ['id', 'username', 'password', 'email', 'register_date']

@admin.register(Review)
class ReviewAdmin(admin.ModelAdmin):
  list_display = ['id', 'rating', 'comment', 'user', 'game', 'date']

@admin.register(GameCollection)
class GameCollectionAdmin(admin.ModelAdmin):
  list_display = ['id', 'id_user', 'is_favorite', 'name', 'image']
