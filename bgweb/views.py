from django.shortcuts import render
from django.http import HttpResponse
from django.http import Http404
from django.shortcuts import redirect
from django.db.models import Q, Count, Sum, Min, Max, Avg
import json
import django.utils.timezone as timezone
from PIL import Image, ImageDraw, ImageFont, ImageFilter

from . import models
from bgweb.forms import UserForm
import random
from html.parser import HTMLParser


def home(request):
    if request.session.get('is_login', None):
        login_state = True
    else:
        login_state = False

    display_content = {
        # display content template
        'user_id': '',
        'username': '',
        'portrait': '',
        'content': None,
    }
    if login_state:  # determine what we should display in home by whether the user has login
        display_content['user_id'] = request.session['user_id']
        display_content['username'] = request.session['username']
        display_content['portrait'] = str(models.User.objects.get(id=request.session['user_id']).image)

        #show game collection
        display_content['game_collection'] = models.GameCollection.objects.filter(id_user=request.session['user_id'])
    else:
        pass

    #get all games from DB
    display_content['content'] = models.Game.objects.all()

    return render(request, 'home.html', {'content': display_content, 'login': login_state})


def game_detail(request, game_id):
    game = {
        'id': str(game_id),
        'name': '',
        'creator': '',
        'image': '',
        'rating': 0,
        'rating_from_how_many_reviews': 0,
        'show_expension': False,
        'description': '',
        'is_login': request.session.get('is_login', None),
        'user_id': '',
        'expension': [],
        'reviews': None,
    }
    try:
        #get the selected id from games
        game_obj = models.Game.objects.get(id=game_id)
        expension = game_obj.expension_game.all()

        if game_obj.expansion is not None and len(expension) > 0:
            game['name'] = game_obj.name + ' [' + game_obj.expension.name + ']'
        else:
            game['name'] = game_obj.name

        reviews_objs = models.Review.objects.filter(game=game_obj)

        game['reviews'] = reviews_objs

        rating_from_game = reviews_objs.values('game').annotate(Avg('rating'))
        if len(rating_from_game):
            game['rating'] = round(rating_from_game[0]['rating__avg'], 1)
        else:
            game['rating'] = 0

        num_reviews = reviews_objs.values('game').annotate(Count('id'))
        if len(num_reviews):
            game['rating_from_how_many_reviews'] = num_reviews[0]['id__count']
        else:
            game['rating_from_how_many_reviews'] = 0

        if game['is_login']:
            game['user_id'] = request.session['user_id']

        game['creator'] = game_obj.creator
        game['image'] = game_obj.image
        game['show_expension'] = game_obj.expansion is None and len(expension) > 0
        game['description'] = game_obj.description
        game['expension'] = expension

    except models.Game.DoesNotExist:
        raise Http404('Game not found')
    return render(request, 'game_detail.html', {'game': game})


def game_catalogue(request):
    category = models.Category.objects.all()
    game_objs = models.Game.objects.all()

    games = []
    for game_obj in game_objs:
        if game_obj.expansion is None:
            game_obj_name = game_obj.name
        else:
            game_obj_name = game_obj.name + ' [' + str(game_obj.expansion.name) + ']'

        reviews_objs = models.Review.objects.filter(game=game_obj)

        rating_query = reviews_objs.values('game').annotate(Avg('rating'))
        if len(rating_query):
            rating = rating_query[0]['rating__avg']
        else:
            rating = 0

        num_reviews_query = reviews_objs.values('game').annotate(Count('id'))
        if len(num_reviews_query):
            num_reviews = num_reviews_query[0]['id__count']
        else:
            num_reviews = 0


        games.append({
            'id': game_obj.id,
            'name': game_obj_name,
            'appendage': game_obj.expansion is not None,
            'rating': round(rating, 1),
            'num_reviews': num_reviews,
        })

    display_content = {
        'categories':  category,
        'games': sorted(games, key=lambda x: x['rating'], reverse=True),
        'count': len(games),
    }

    return render(request, 'game_catalogue.html', {'content': display_content})


def update_category(request, category_id):
    game_objs = models.Game.objects.filter(Q(category=category_id) | Q(expansion__category=category_id))
    category = models.Category.objects.all()

    games = []
    for game_obj in game_objs:
        if game_obj.expansion is None:
            game_obj_name = game_obj.name
        else:
            game_obj_name = game_obj.name + ' [' + str(game_obj.expansion.name) + ']'

        reviews_objs = models.Review.objects.filter(game=game_obj)

        rating_query = reviews_objs.values('game').annotate(Avg('rating'))
        if len(rating_query):
            rating = rating_query[0]['rating__avg']
        else:
            rating = 0

        num_reviews_query = reviews_objs.values('game').annotate(Count('id'))
        if len(num_reviews_query):
            num_reviews = num_reviews_query[0]['id__count']
        else:
            num_reviews = 0


        games.append({
            'id': game_obj.id,
            'name': game_obj_name,
            'appendage': game_obj.expansion is not None,
            'rating': round(rating, 1),
            'num_reviews': num_reviews,
        })

    display_content = {
        'categories': category,
        'games': sorted(games, key=lambda x: x['rating'], reverse=True),
        'count': len(games),
    }

    return render(request, 'game_catalogue.html', {'content': display_content})


def login_page(request):
    if request.session.get('is_login', None):  # Not allow login again
        return redirect('home')
    return render(request, 'login.html')


def login_api(request):
    if request.session.get('is_login', None):  # Not allow login again
        return redirect('home')
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        # Check user here

        try:
            user = models.User.objects.get(username=username)

            if user.password == password:
                request.session['is_login'] = True
                request.session['user_id'] = user.id
                request.session['username'] = username
                return HttpResponse(json.dumps({'status': 'success', 'message': 'login successfully'}))
            else:
                return HttpResponse(json.dumps({'status': 'error', 'message': 'password incorrect'}))
        except:
            return HttpResponse(json.dumps({'status': 'error', 'message': 'No such user'}))



    return HttpResponse(json.dumps({'status': 'error', 'message': 'illegal access'}))


def logout_api(request):
    if not request.session.get('is_login', None):
        return redirect("login")
    request.session.flush()
    return redirect('home')


def register_page(request):
    if request.session.get('is_login', None):  # Should logout to register
        return redirect('home')
    return render(request, 'register.html')


def register_api(request):
    if request.session.get('is_login', None):  # Should logout to register
        return redirect('home')
    if request.method == 'POST':
        result = {
            'status': '',
            'message': None
        }

        username = request.POST['username']
        password = request.POST['password']
        email = request.POST['email']

        same_name_user = models.User.objects.filter(username=username)
        if same_name_user:
            result['status'] = 'error'
            result['message'] = 'User exists'

        same_email_user = models.User.objects.filter(email=email)
        if not same_name_user and same_email_user:
            result['status'] = 'error'
            result['message'] = 'Email has been registered'

        if result['status'] == 'error':
            return HttpResponse(json.dumps(result))
        else:
            new_user = models.User()
            new_user.username = username
            new_user.password = password
            new_user.email = email
            new_user.save()
            result['status'] = 'success'
            result['message'] = 'Register successfully'

            request.session['is_login'] = True
            request.session['user_id'] = new_user.id
            request.session['username'] = username
            return HttpResponse(json.dumps(result))

    return HttpResponse(json.dumps({'status': 'error', 'message': 'illegal access'}))


def add_to_collection_page(request, game_id):
    if request.session.get('is_login', None):
        login_state = True
    else:
        return redirect("login")

    display_content = {
        'count': 0,
        'game_id': game_id,
        'collections': [],
    }

    # get the selected id from games
    collections_objs = models.GameCollection.objects.filter(id_user=request.session['user_id'])
    display_content['count'] = collections_objs.count()
    for collection_obj in collections_objs:
        display_content['collections'].append({
            'id': collection_obj.id,
            'name': collection_obj.name,
            'has_image': collection_obj.image != '',
            'image': collection_obj.image,
            'game_num': len(collection_obj.games.all()),
            'is_favourite': collection_obj.is_favorite,
            'is_in': bool(len(collection_obj.games.filter(id=game_id))),
        })

    return render(request, 'add_to_collection.html', display_content)


def select_collection(request):
    if request.session.get('is_login', None):
        login_state = True
    else:
        return redirect("login")

    if request.method == 'POST' and login_state:
        select = request.POST['select']
        game_id = request.POST['game_id']

        for collection_id in select.split(','):
            try:
                collection = models.GameCollection.objects.get(id=collection_id)
                collection.games.add(models.Game.objects.get(id=game_id))
            except:
                raise Http404('Illegal access')

        return HttpResponse(json.dumps({'status': 'success', 'message': 'upload successfully'}))
    return HttpResponse(json.dumps({'status': 'error', 'message': 'illegal access'}))


def remove_from_collection(request):
    if request.session.get('is_login', None):
        login_state = True
    else:
        return redirect("login")

    if request.method == 'POST':
        item_id = int(request.POST.get('item_id'))
        collection_id = int(request.POST.get('collection_id'))
        collection = models.GameCollection.objects.get(id=collection_id)
        item = models.Game.objects.get(id=item_id)
        collection.games.remove(item_id)

    return redirect("collection_detail", collection_id)


def my_collection(request):
    if request.session.get('is_login', None):
        login_state = True
    else:
        return redirect("login")

    # get the selected id from games
    collections_objs = models.GameCollection.objects.filter(id_user=request.session['user_id'])
    collections = []
    for collection_obj in collections_objs:
        collections.append({
            'id': collection_obj.id,
            'name': collection_obj.name,
            'has_image': collection_obj.image != '',
            'image': collection_obj.image,
            'game_num': len(collection_obj.games.all()),
            'is_favourite': collection_obj.is_favorite,
        })

    return render(request, 'my_collection.html', {'collections': collections})


def collection_detail(request, collection_id):
    if request.session.get('is_login', None):
        login_state = True
    else:
        return redirect("login")

    try:
        collection_obj = models.GameCollection.objects.get(id=collection_id)
        collection = {
            'id': collection_obj.id,
            'name': collection_obj.name,
            'is_favorite': collection_obj.is_favorite,
            'has_image': collection_obj.image != '',
            'image': collection_obj.image,
            'games': collection_obj.games.all(),
        }

    except models.Game.DoesNotExist:
        raise Http404('Game not found')
    return render(request, 'collection_detail.html', collection)


def edit_collection(request, collection_id):
    if request.session.get('is_login', None):
        login_state = True
    else:
        return redirect("login")

    collection = models.GameCollection.objects.get(id=collection_id)
    print(collection.image != '')
    result = {
        'id': collection_id,
        'name': collection.name,
        'image': collection.image,
        'is_favourite': collection.is_favorite,
        'has_image': collection.image != '',
    }

    return render(request, 'edit_collection.html', result)


def create_collection(request):
    if request.session.get('is_login', None):
        login_state = True
    else:
        return redirect("login")

    return render(request, 'create_collection.html')


def upload_collection(request):
    if request.session.get('is_login', None):
        login_state = True
    else:
        return redirect("login")

    if request.method == 'POST':
        user_id = int(request.session['user_id'])

        try:
            collection = models.GameCollection.objects.get(id=request.POST.get('id'))
            is_favorite = len(request.POST.getlist('is_favourite'))
            name = request.POST['name']
            image = request.FILES.get('img')
            collection.is_favorite = is_favorite
            collection.name = name
            collection.image = image
            collection.save()
        except:
            print('here')
            collection = models.GameCollection(
                id_user=models.User.objects.get(id=user_id),
                is_favorite=len(request.POST.getlist('is_favourite')),
                name=request.POST['name'],
                image=request.FILES.get('img'),
            )


        collection.save()
        return HttpResponse(json.dumps({'status': 'success', 'message': 'upload successfully'}))
    return HttpResponse(json.dumps({'status': 'error', 'message': 'illegal access'}))


def delete_collection(request, collection_id):
    if request.session.get('is_login', None):
        login_state = True
    else:
        return redirect("login")

    try:
        collection = models.GameCollection.objects.get(id=collection_id)
        collection.delete()

    except models.Game.DoesNotExist:
        raise Http404('Game not found')
    return redirect('my_collection')


def user_profile_api(request):
    if request.session.get('is_login', None):
        login_state = True
    else:
        return redirect("login")

    user_id = request.session['user_id']
    user = models.User.objects.get(id=user_id)
    if request.method == 'POST':
        form = UserForm(request.POST)   
        if form.is_valid():
            model_instance = form.save(commit=False)
            model_instance.timestamp = timezone.now()
            model_instance.save()
            return redirect('/')
    else:
        
        form = UserForm({'name' : user.username, 'email' : user.email})
    return render(request, 'profile.html', {'form': form})


def add_reviews_api(request):
    if request.session.get('is_login', None):
        login_state = True
    else:
        return redirect("login")

    if request.method == 'POST':
        user_id = request.session['user_id']
        game_id = request.POST['game_id']
        user = models.User.objects.get(id=user_id)
        game = models.Game.objects.get(id=game_id)
        rating = request.POST['rating']
        review = request.POST['review']
        models.Review(
            rating=rating,
            comment=review,
            user=user,
            game=game,
        ).save()
        return HttpResponse(json.dumps({'status': 'success', 'message': 'upload successfully'}))
    return HttpResponse(json.dumps({'status': 'error', 'message': 'illegal access'}))


def search(request):
    if request.session.get('is_login', None):
        login_state = True
    else:
        login_state = False

    display_content = {
        # display content template
        'user_id': '',
        'username': '',
        'content': [],
        'login': login_state,
        'count': 0,
        'q': ''
    }
    if login_state:  # determine what we should display in home by whether the user has login
        display_content['user_id'] = request.session['user_id']
        display_content['username'] = request.session['username']
    else:
        pass

    #get all games from DB
    q = request.GET['q']
    display_content['q'] = q
    game_objs = models.Game.objects.filter(Q(name__icontains=q) | Q(description__icontains=q))

    games = []
    for game_obj in game_objs:
        if game_obj.expansion is None:
            game_obj_name = game_obj.name
        else:
            game_obj_name = game_obj.name + ' [' + str(game_obj.expansion.name) + ']'

        reviews_objs = models.Review.objects.filter(game=game_obj)

        rating_query = reviews_objs.values('game').annotate(Avg('rating'))
        if len(rating_query):
            rating = rating_query[0]['rating__avg']
        else:
            rating = 0

        num_reviews_query = reviews_objs.values('game').annotate(Count('id'))
        if len(num_reviews_query):
            num_reviews = num_reviews_query[0]['id__count']
        else:
            num_reviews = 0

        games.append({
            'id': game_obj.id,
            'name': game_obj_name,
            'appendage': game_obj.expansion is not None,
            'rating': round(rating, 1),
            'num_reviews': num_reviews,
        })

    display_content['content'] = sorted(games, key=lambda x: x['rating'], reverse=True)
    display_content['count'] = len(display_content['content'])

    return render(request, 'search.html', display_content)
