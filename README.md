# Borad game web app - for SD cw2

This is a prototype of product for SD coursework submission 2. 

## Environment setup

[Install Django 3.0.4](https://docs.djangoproject.com/en/3.0/intro/install/)

[Install Django Clean up] ```$pip install django-cleanup```
## Quick started

After setup and enter the project directory, we can run the backend on local machine by running

```
$ python manage.py runserver
Watching for file changes with StatReloader
Performing system checks...

System check identified no issues (0 silenced).

You have 17 unapplied migration(s). Your project may not work properly until you apply the migrations for app(s): admin, auth, contenttypes, sessions.
Run 'python manage.py migrate' to apply them.
March 04, 2020 - 16:44:36
Django version 3.0.4, using settings 'BoardGame.settings'
Starting development server at http://127.0.0.1:8000/
Quit the server with CTRL-BREAK.

```

We can access the website by url `http://localhost:8000/`. We can change the port by adding parameter 
after running command like `python manage.py runserver localhost:80`, then it becomes `80` port.

Further document and tutorial could be found in [Django official website](https://www.djangoproject.com/)

## Template and static file

There is a template and static configuration in `settings.py` file, making the Django could find the `.html`
and static file automatically. From now on, we can just simply put all `.html` file in `BoardGame/template` and 
all static files in `BoardGame/static` according to its file format.